/* eslint-disable array-callback-return */
/* eslint-disable no-useless-constructor */
import React from 'react';
import { Grid, TextField } from '@material-ui/core';
import { MultiSelect } from 'primereact/multiselect';
import {Card} from 'primereact/card';
import axios from 'axios';
import _ from 'lodash';

class FunitureList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            deliveryValue: [],
            furnitureStyleValue: [],
            product: [],
            furnitureStyle: [],
            dataDisplay: [],
            textSearch: '',
            delivery: [
                { label: '1 Week', value: [1, 7] },
                { label: '2 Weeks', value: [8, 14] },
                { label: '1 Month', value: [15, 31] },
                { label: 'Lainnya', value: [32, 0] }
            ],
        };

    }

    async componentDidMount() {
        await axios.get('http://www.mocky.io/v2/5c9105cb330000112b649af8')
        .then(fetchData => {
            const furnitureStyle = [];
            for (const data of fetchData.data.furniture_styles) {
                furnitureStyle.push({ label: data, value: data });
            }
            this.setState({
                furnitureStyle,
                product: fetchData.data.products,
                dataDisplay: fetchData.data.products,
            });
        }).catch(err => {
            alert('Failed to fetch data, please try again');
        });
    }

    onSearch = () => {
        const { 
            textSearch,
            product,
            furnitureStyleValue,
            deliveryValue,
        } = this.state;
        let dataDisplay = product;
 
        if (textSearch.length > 0) {
            let stringName = textSearch.toLowerCase();
            dataDisplay = dataDisplay.filter(function(el) {
                const searchValue = el.name.toLowerCase();
                return searchValue.indexOf(stringName) !== -1;
            });
        }

        if (furnitureStyleValue.length > 0) {
            dataDisplay = dataDisplay.filter(function(val) {
                const dataResult = val.furniture_style.filter(function(fs) {
                    return furnitureStyleValue.includes(fs); 
                });
                return dataResult.length > 0;
            });
        }

        if (deliveryValue.length > 0) {
            const tempData = [];
            dataDisplay.map((val, key) => {
                const check = this.filterDeliverCheck(Number(val.delivery_time));
                if (check) {
                    tempData.push(val);
                }
            });
            dataDisplay = tempData;
        }

        this.setState({ dataDisplay });
    }

    filterDeliverCheck = (value) => {
        const { deliveryValue } = this.state;
        let flag = 0;
        // eslint-disable-next-line array-callback-return
        deliveryValue.map((val, key) => {
            if (val[1] !== 0) {
                if (value >= val[0] && value <= val[1]) {
                    flag = 1;
                    return false;
                }
            } else {
                if (value >= val[0]) {
                    flag = 1;
                    return false;
                }
            }
        });

        return flag === 0 ? false : true;
    
    }

    onKeyPress = (e) => {
        if (e.key === 'Enter') {
            this.onSearch();
        }
    }

    renderProduct = () => {
        const data = [];
        const { dataDisplay } = this.state;

        // eslint-disable-next-line array-callback-return
        dataDisplay.map((val, key) => {
            const desc = val.description.length > 144 ? `${val.description.substring(0, 144)} ...` : val.description;
            const furStyle = _.join(val.furniture_style, ', ');
            data.push(
                <Grid item key={key} xs={6}>
                    <Card className="card-list" style={{width: '360px'}}>
                        <Grid container spacing={2}>
                            <Grid item xs={6}>
                                <span className="title">{val.name}</span>
                            </Grid>

                            <Grid item className="text-right" xs={6}>
                                <span className="price">IDR {new Intl.NumberFormat('ID').format(val.price)}</span>
                            </Grid>

                            <Grid item xs={12}>
                                {desc}
                                <br />
                                <br />
                                <span className="furniture-style">{furStyle}</span>
                            </Grid>
                            <Grid item xs={12} className="text-right">
                                <span className="delivery-time">{val.delivery_time} Days</span>
                            </Grid>
                            
                        </Grid>
                    </Card>
                </Grid>
            )
        });

        if (data.length === 0) {
            data.push(<center>No Data Found</center>)
        }
        return data;
    }

    onChangeFurniture = async (e) => {
        await this.setState({ furnitureStyleValue: e.value });
        this.onSearch();
    }
 
    onChangeDelivery = async (e) => {
        await this.setState({ deliveryValue: e.value });
        this.onSearch();
    }

    render() {
        const { 
            furnitureStyle,
            textSearch,
            product,
            delivery,
            furnitureStyleValue,
            deliveryValue 
        } = this.state;
        
        return (
            <div className="container">
                {
                    product.length > 0 ? (
                    <React.Fragment>
                        <div className="header">
                            <Grid
                                container
                                direction="row"
                                justify="flex-start"
                                alignItems="center"
                                spacing={2}
                            >
                                <Grid item xs={12}>
                                    <TextField value={textSearch} onKeyPress={this.onKeyPress} onChange={(e) => this.setState({ textSearch: e.target.value }) } className="search-text" placeholder="Search Furniture" />
                                </Grid>
                                <Grid item xs={6}>
                                    <MultiSelect value={furnitureStyleValue} options={furnitureStyle} onChange={this.onChangeFurniture}
                                            style={{width:'20em'}} filter={true} placeholder="Furniture Style" />
                                </Grid>
                                <Grid item xs={6}>
                                    <MultiSelect value={deliveryValue} options={delivery} onChange={this.onChangeDelivery}
                                            style={{width:'20em'}} filter={true} placeholder="Delivery Time" />
                                </Grid>
                            </Grid>
                        </div>

                        <div className="content">
                            <Grid
                                container
                                direction="row"
                                justify="flex-start"
                            >
                                {this.renderProduct()}
                            </Grid>
                        </div>
                    </React.Fragment>
                    ) : (
                        <div>Loading ...</div>
                    )
                }
            </div>
        );
    }
}

export default FunitureList;
