/* eslint-disable react/react-in-jsx-scope */
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom';
import React from 'react';
import FunitureList from '../pages/FunitureList';

const Routing = () => (
    <Router>
        <Switch>
            <Route exact path="/" component={FunitureList} />
        </Switch>
    </Router>
);

export default Routing;